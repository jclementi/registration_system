registration system readme
==========================

This project was created for a class on Object-Oriented design and modeling. To complete the project, each student had to implement a subset of the functionality described in `project_description.md`. The requirements there describe a system that handles all aspects of course registration for a hypothetical university. This implementation chooses to focus on system administration and student registration. Using the command-line interface, an administrator user can edit the fields of database records and prepare registration sessions for student registration. A student user may register for any sections that have been made avaliable to the current registration session.

> Note: The diagrams in the `models` directory give more information about some of the different patterns put to use in this project.


## running the program

Before running the program, you have to install the dependencies in the pipfile and install the current directory as a package so that the files can find each other. To do that run these two commands:

```bash
    $ pipenv install '-e .' --dev
    $ pipenv install
```

After installing, you can use `make run` to run the program. To use as a student, select one of the students presented on the menu screen. To use as an admin, input `a` at the first menu screen. `q` will take you back to the last menu except when inputting data, where `q` will be interpreted as the data you want to enter.

To run the tests, use `make test`.


### notes for usage

I only included a small amount of input validation, and that validation does not extend to checking the database for duplicate records. In most instances, things will keep working fine even if there are duplicates, but I've not tested to ensure that's the case. Instead, the program trusts the user to input values that make sense. This extends to these scenarios:

* when entering values that link to foreign keys, those values must exist in the remote table
* a user may delete values that are referenced in other records
 * for example: deleting a division that is referenced by a course results in the course listing "None" as its division

SQL statements are not rigorously escaped. If you want to, you can easily break the database through sql injection. This program assumes the user will be friendly. To play well with the unescaped statements, avoid using quotes or apostrophes in string fields.


## design

Modeling and design was a very important component of this project. I've included two essential use cases used to describe some workflows the project implements, and I've also included diagrams showing several of the patterns used in the project.


### essential use cases

These are two essential use cases presenting details of the core workflows for both student users and administrative users.

#### students register for courses

*Description:* A student will use the system to register for a course. A student may modify their existing course registration by dropping a course or adding a new one.

*Actors:*

* Student

*Basic Flow:*

* 1.1 - 'log on' - student logs into the system
* 1.2 - 'search' - student searches for desired courses
* 1.3 - 'select' - student selects an available course section to add
* 1.4 - 'commit' - the course is added to the student's schedule

*Alternate Flows:*

* 2.1 - 'log on' - user wishes to remove a course
 * student views current schedule
 * student selects a course to remove
 * the course is removed from the student's schedule
* 2.2 - 'select' -  course requires instructor consent
 * instructor is notified of the student's request to register for the course
 * course is marked 'tentative' pending instructor's decision
 * course is added if approved by the instructor, otherwise, the course is removed
* 2.3 - 'select' - student is already registered for max courses
 * student may swap courses they are enrolled in for the selected course
 * if they do, the course is added and the swapped course is dropped, otherwise
 * department chair and academic advisor are notified of student's request to register
 * the registration is marked as 'pending' pending deptartment chair's decision
 * course is added if approved by the department chair
* 2.4 - 'select' - selected course requires labs
 * student registers for an available lab session
* 2.5 - 'select' - student has not fulfilled prerequisites
 * student is notified of their missing prerequisite
 * student cannot add the selected course
* 2.6 - 'search' - student has an administrative restriction
 * student is notified of their academic restriction
 * student cannot select courses to add
* 2.7 - 'log on' - student is not authenticated
 * student may attempt to authenticate again
* 2.8 - 'log on' - registration is closed
 * student is notified that registration is closed
 * student cannot log into the system


#### system administrator maintains records

*Description:* An administrator logs in to modify current course offerings, maintain student and instructor records, open and close registration periods, and delete courses that don't meet certain requirements.

*Actors:*

* Administrator

*Basic Flow:*

* 1.1 - 'log on' - admin logs into the system
* 1.2 - 'select record' - admin selects records to modify
* 1.3 - 'modify details' - admin submits new details for the record
* 1.4 - 'change committed' - record is updated

*Alternate Flows:*

* 2.1 - 'log on' - user does not have admin permissions
 * user is notified they do not have correct permissions
 * user cannot log on as administrator
* 2.2 - 'log on' - user is not authenticated
 * user may attempt to log in again
* 2.3 - 'select record' - admin adds a record
 * admin selects the type of record they'd like to add
 * admin submits the appropriate details for the record
 * the record is added
* 2.3 - 'modify details' - admin deletes a record
 * admin chooses to delete a record
 * the chosen record is deleted
* 2.4 - 'log on' - administrator closes registration
 * registration is closed
* 2.5 - 'change committed' - the information submitted doesn't pass validation
 * administrator may modify and re-submit details or abandon the change
 * when details pass validation, the change is committed


### patterns used

The patterns used in the project are described and diagrammed in the scans included in the `models` folder.

In addition to the patterns in those scans, the project makes use of the "Template" pattern in the `dbConnector` object. This object is the superclass for all database connector objects. It defines routines divided into several subroutines. `Insert` is the best example of this. This method is divided into four stages:

* `prepInsert`
* `resolveFks`
* `insertDict`
* `insertLinkTables`

Together, these functions form a template that subclasses may modify by overriding. Most tables must override `resolveFks` because the foreign keys they need to resolve are specific to each table, and some require special processing that the superclass would have no way of anticipating.


## initial data

Run `make dbinit` to initialize the database with the data described below. MySQL throws an exception related to cleanup after the initialization happens, but it doesn't affect the records inserted. This makes the data available immediately upon starting the application.

```
divisions:
* computer science
* physical science
* humanities

majors:
* physics
* math
* humanities

registration sessions:
* spring 2018
* autumn 2018

courses:
* cmsc150 - no restrictions
* cmsc151 - prereq: cmsc150
* phys150 - instructor consent
* phis151 - instructor consent, prereq: phys150

sections:

> note: All sections have been made available to the 'autumn 2018' registration session. None have been made available to the 'spring 2018' session, so you can test adding sections and courses there, if you like.

* cmsc150-01 - nothing special
* cmsc150-02 - only has one seat, already filled
* cmsc151-01 - section has a lab section
* cmsc151-01lab - must be registered for this before 'cmsc151-01'
* phys150-01 - nothing special
* phys150-02 - nothing special

students:
* sam - registered for no courses
* sacha - registered for several courses

instructors:
* hector - teaching cmsc150-01, cmsc150-02, cmsc151-01
* helen - teaching no sections
