.PHONY: test run dbinit

test:
	pipenv run python -m unittest discover

run:
	pipenv run python regis/cli.py

dbinit:
	pipenv run python regis/regisDb.py
