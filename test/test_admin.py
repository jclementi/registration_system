import unittest

import regis.regiSys
from regis.regiSys import *
import regis.regisDb
from regis.regisDb import RegisDb


class TestSysAdmin(unittest.TestCase):

    def setUp(self):
        db = RegisDb()
        db.initializeDatabase()

    # instuctor tests
    instructorParams = {
            "name": "tom",
    }

    def test_modifyInstructor(self):
        params = {"name": "elmer",
                "divisions": ['computer science'],
                }
        i = Instructor(params)
        i.save()

        i = Instructor.fetch(i.name)
        self.assertTrue(i is not None)

        sec = Section({
            "name": "phys151-20",
            "registrationSession": "autumn 2018",
            "schedule": "mwf",
            "totalSeats": 10,
            "room": "kent 102",
            "isLab": 0,
            "courseNumber": "phys151",
        })
        sec.save()

        i.modify({"name": "steve",
            "divisions": ['humanities', 'physical science'],
            "sections": ['phys151-20']
        })

        modInstr = Instructor.fetch(i.name)

        self.assertEqual(modInstr.name, "steve")
        self.assertTrue('humanities' in modInstr.divisions)
        self.assertTrue('physical science' in modInstr.divisions)
        self.assertTrue('computer science' not in modInstr.divisions)

        sec = Section.fetch('phys151-20')
        self.assertTrue('steve' in sec.instructors)


    def test_addInstructor(self):
        params = {
                "name": "mark",
                "divisions": ['computer science', 'humanities'],
                }
        i = Instructor(params)
        i.save()
        
        instr = Instructor.fetch("mark")
        self.assertTrue(instr is not None)
        self.assertEqual(instr.name, "mark")
        self.assertTrue('computer science' in instr.divisions)
        self.assertTrue('humanities' in instr.divisions)


    def test_validateNewInstructor(self):
        params = {
                "name": "steve",
        }
        isValid, invalidParams = Instructor.validate(params)
        self.assertTrue(isValid)

        params = {
                "name": 20,
        }

        isValid, invalidParams = Instructor.validate(params)
        self.assertFalse(isValid)
        self.assertTrue("name" in invalidParams)

    # waiting until RegistrationSessions are implemented
    def test_deleteInstructor(self):
        params = {
                "name": "mark",
                "divisions": ['computer science', 'humanities'],
                "sections": ['phys151-01'],
                }
        i = Instructor(params)
        i.save()
        
        instr = Instructor.fetch("mark")
        self.assertTrue(instr is not None)
        i.delete()
        instr = Instructor.fetch("mark")
        self.assertTrue(instr is None)

        sec = Section.fetch('phys151-01')
        self.assertTrue("mark" not in sec.instructors)
        self.assertEqual(sec.instructors, [])



    # student tests

    studentParams = {
            "name": "shannon",
            "status": "full-time",
            "graduationDate": "June 2018",
            "majors": ['physics'],
            "sections": [('phys151-01', 'official')],
    }

    def test_modifyStudent(self):
        params = TestSysAdmin.studentParams
        s = Student(params)
        s.save()
        s.modify({
                "status": "part-time",
                "graduationDate": "June 2019",
                "sections": [('cmsc151-01', 'official'), 
                    ('phys151-01', 'tentative')],
                "previousCourses": ['phys151'],
        })

        student = Student.fetch(s.name)
        self.assertEqual(student.name, "shannon")
        self.assertEqual(student.status, "part-time")
        self.assertEqual(student.graduationDate, "June 2019")
        self.assertEqual(student.previousCourses, ['phys151'])
        self.assertTrue(('cmsc151-01', 'official') in student.sections)
        self.assertTrue(('phys151-01', 'tentative') in student.sections)

        sec = Section.fetch('cmsc151-01')
        self.assertTrue(('shannon', 'official') in sec.students)

        sec = Section.fetch('phys151-01')
        self.assertTrue(('shannon', 'tentative') in sec.students)


    def test_addStudent(self):
        params = {
                "name": "sven",
                "status": "full-time",
                "graduationDate": "June 2018",
                "majors": ['physics'],
                "sections": [('phys151-01', 'official')],
        }
        s = Student(params)
        s.save()

        student = Student.fetch(params['name'])
        self.assertTrue(student is not None)
        self.assertEqual(student.name, "sven")
        self.assertEqual(student.status, "full-time")
        self.assertEqual(student.graduationDate, "June 2018")
        self.assertEqual(student.majors, ['physics'])
        self.assertEqual(student.sections, [('phys151-01', 'official')])


    def test_validateNewStudent(self):
        isValid, invalidParams = Student.validate(
                TestSysAdmin.studentParams)
        
        self.assertTrue(isValid)
        self.assertEqual(invalidParams, [])

        badStudentParams = {
                "status": ["one-time", "two-time"],
                "graduationDate": 20,
        }

        isValid, invalidParams = Student.validate(
                badStudentParams)
        self.assertFalse(isValid)
        self.assertTrue("status" in invalidParams)
        self.assertTrue("graduationDate" in invalidParams)

    # waiting until RegistrationSessions are implemented
    def test_deleteStudent(self):
        params = TestSysAdmin.studentParams
        s = Student(params)
        s.save()

        student = Student.fetch(params['name'])
        self.assertTrue(student is not None)

        student.delete()

        student = Student.fetch(params['name'])
        self.assertTrue(student is None)

        sec = Section.fetch('phys151-01')
        self.assertTrue(params['name'] not in sec.students)
        self.assertTrue(None not in sec.students)


    # course tests
    courseParams = {
            "courseNumber": "cmsc158",
            "division": "computer science",
            "subject": "OOP",
            "description": "object-oriented programming",
            "consentRequired": False,
            "prerequisiteCourses": ['cmsc150', 'cmsc151'],
    }

    def test_modifyCourse(self):
        params = TestSysAdmin.courseParams
        c = Course(params)
        c.save()
        c.modify({
                "division": "physical science",
                "description": "just normal programming",
                "prerequisiteCourses": [],
        })

        course = Course.fetch(params['courseNumber'])
        self.assertEqual(course.division, "physical science")
        self.assertEqual(course.subject, "OOP")
        self.assertEqual(course.description, "just normal programming")
        self.assertEqual(course.prerequisiteCourses, [])
        self.assertEqual(course.consentRequired, False)


    def test_validateNewCourse(self):
        #cv = Course.getValidator()
        #isValid, invalidParams = cv.validate(
        #        TestSysAdmin.courseParams)
        isValid, invalidParams = Course.validate(TestSysAdmin.courseParams)
        self.assertTrue(isValid)
        self.assertEqual(invalidParams, [])

        badCourseParams = {
                "courseNumber": 204,
                "division": "Physical Sciences",
                "subject" : ["Lose More"],
                "description" : "This description is valid",
                "consentRequired": True,
        }

        isValid, invalidParams = Course.validate(badCourseParams)
        self.assertFalse(isValid)
        self.assertTrue("courseNumber" in invalidParams)
        self.assertTrue("division" not in invalidParams)
        self.assertTrue("subject" in invalidParams)
        self.assertTrue("description" not in invalidParams)
        self.assertTrue("consentRequired" not in invalidParams)


    def test_addCourse(self):
        params = TestSysAdmin.courseParams
        c = Course(params)
        c.save()
        course = Course.fetch(params['courseNumber'])
        self.assertTrue(course is not None)
        self.assertEqual(course.courseNumber, "cmsc158")
        self.assertEqual(course.division, "computer science")
        self.assertEqual(course.subject, "OOP")
        self.assertEqual(course.description, "object-oriented programming")
        self.assertEqual(course.consentRequired, False)
        self.assertEqual(course.prerequisiteCourses, ['cmsc150', 'cmsc151'])
        

    # waiting until RegistrationSessions are implemented
    def test_deleteCourse(self):
        params = TestSysAdmin.courseParams
        c = Course(params)
        c.save()
        course = Course.fetch(params['courseNumber'])
        self.assertTrue(course is not None)
        course.delete()

        course = Course.fetch(params['courseNumber'])
        self.assertTrue(course is None)


    # section tests

    sectionParams = {
            "name": "cmsc150-03",
            "registrationSession": "autumn 2018",
            "courseNumber": "cmsc150",
            "schedule": "mwf",
            "totalSeats": 10,
            "room": "pick 305",
            "isLab": False,
    }

    def test_addSection(self):
        params = TestSysAdmin.sectionParams
        s = Section(params)
        s.save()

        section = Section.fetch(params['name'])
        self.assertTrue(section is not None)
        self.assertEqual(section.courseNumber, "cmsc150")
        self.assertEqual(section.schedule, "mwf")
        self.assertEqual(section.totalSeats, 10)
        self.assertEqual(section.room, "pick 305")
        self.assertEqual(section.isLab, False)
        
    def test_modifySection(self):

        params = {
                "name": "cmsc150-03",
                "registrationSession": "autumn 2018",
                "courseNumber": "cmsc150",
                "schedule": "mwf",
                "totalSeats": 10,
                "room": "pick 305",
                "isLab": False,
        }
        section = Section(params)
        section.save()

        params = {"name": "elmer",
                "divisions": ['computer science'],
                }
        instr = Instructor(params)
        instr.save()

        section.modify({
            "name": "phys151-04",
            "courseNumber": "phys151", 
            "schedule": "tth",
            "instructors": ['elmer'],
            "room": "eckhart 205"})

        modSection = Section.fetch("phys151-04")

        self.assertEqual(modSection.courseNumber, "phys151")
        self.assertEqual(modSection.name, "phys151-04")
        self.assertEqual(modSection.schedule, "tth")
        self.assertEqual(modSection.totalSeats, 10)
        self.assertEqual(modSection.room, "eckhart 205")
        self.assertEqual(modSection.isLab, False)
        self.assertTrue('elmer' in modSection.instructors)

        modInstr = Instructor.fetch('elmer')
        self.assertTrue('phys151-04' in modInstr.sections)


    def test_validateNewSection(self):
        isValid, invalidParams = Section.validate(
                TestSysAdmin.sectionParams)

        badSectionParams = {
                "name": 20,
                "registrationSession": "okay",
                "courseNumber": [10, 20],
                "schedule": ["m", "w", "f"],
                "totalSeats": "billions",
                "room": "Kent 210",
                "isLab": True,
                "instructors": ["o", "k"],
                "students": "not okay",
        }

        isValid, invalidParams = Section.validate(
                badSectionParams)
        self.assertFalse(isValid)
        self.assertTrue("name" in invalidParams)
        self.assertTrue("registrationSession" not in invalidParams)
        self.assertTrue("courseNumber" in invalidParams)
        self.assertTrue("schedule" in invalidParams)
        self.assertTrue("totalSeats" in invalidParams)
        self.assertTrue("room" not in invalidParams)
        self.assertTrue("isLab" not in invalidParams)
        self.assertTrue("instructors" not in invalidParams)
        self.assertTrue("students" in invalidParams)


    # waiting until RegistrationSessions are implemented
    def test_deleteSection(self):
        params = TestSysAdmin.sectionParams
        s = Section(params)
        s.save()

        section = Section.fetch(params['name'])
        self.assertTrue(section is not None)
        section.delete()

        section = Section.fetch(params['name'])
        self.assertTrue(section is None)

    # tests for registrationSession
    def test_addRegSession(self):
        regParams = {
                "name": "winter 2019",
                "startDate": "01-2019",
                "endDate": "04-2019",
                "isOpen": 0,
                }
        rsess = RegistrationSession(regParams) 
        rsess.save()

        rsess = RegistrationSession.fetch(regParams['name'])

        self.assertEqual(rsess.name, "winter 2019")
        self.assertEqual(rsess.startDate, "01-2019")
        self.assertEqual(rsess.endDate, "04-2019")
        self.assertEqual(rsess.isOpen, False)

    def test_deleteRegSession(self):
        regParams = {
                "name": "winter 2019",
                "startDate": "01-2019",
                "endDate": "04-2019",
                "isOpen": 0,
                }
        rsess = RegistrationSession(regParams) 
        rsess.save()

        rs = RegistrationSession.fetch(regParams['name'])
        self.assertTrue(rs is not None)
        rs.delete()

        rs = RegistrationSession.fetch(regParams['name'])
        self.assertTrue(rs is None)


    def test_modifyRegSession(self):
        regParams = {
                "name": "winter 2019",
                "startDate": "01-2019",
                "endDate": "04-2019",
                "isOpen": 0,
                "courses": ['cmsc150', 'cmsc151'],
                }
        rsess = RegistrationSession(regParams)
        rsess.save()
        rsess.modify({
            "name": "summer 2020",
            "startDate": "04-2020",
            "endDate": "06-2020",
            "courses": [],
            })

        modReg = RegistrationSession.fetch(rsess.name)
        self.assertEqual(modReg.name, "summer 2020")
        self.assertEqual(modReg.startDate, "04-2020")
        self.assertEqual(modReg.endDate, "06-2020")
        self.assertEqual(modReg.isOpen, 0)
        self.assertEqual(modReg.courses, [])


    def test_addAvailableCourse(self):
        regParams = {
                "name": "winter 2019",
                "startDate": "01-2019",
                "endDate": "04-2019",
                "isOpen": 0,
                }

        reg = RegistrationSession(regParams)
        reg.save()

        course1 = Course({
                "courseNumber": "cmsc157",
                "division": "computer science",
                "subject": "OOP",
                "description": "object-oriented programming",
                "consentRequired": False,
                })
        course1.save()

        course2 = Course({
                "courseNumber": "cmsc158",
                "division": "computer science",
                "subject": "OOP-er",
                "description": "more object-oriented programming",
                "consentRequired": False,
                })
        course2.save()
        
        AdminController.addCourse(reg, course1)
        AdminController.addCourse(reg, course2)

        reg = RegistrationSession.fetch(reg.name)
        self.assertTrue(course1.courseNumber in reg.courses)
        self.assertTrue(course2.courseNumber in reg.courses)


    def test_addAvailableSection(self):
        regParams = {
                "name": "winter 2019",
                "startDate": "01-2019",
                "endDate": "04-2019",
                "isOpen": 0,
                }

        reg = RegistrationSession(regParams)
        reg.save()

        course1 = Course({
                "courseNumber": "cmsc157",
                "division": "computer science",
                "subject": "OOP",
                "description": "object-oriented programming",
                "consentRequired": False,
                })
        course1.save()
        section1 = Section({
            "name": "cmsc157-01",
            "courseNumber": "cmsc157",
            "schedule": "mwf",
            "totalSeats": 10,
            "room": "ryerson 210",
            "isLab": False,
        })
        section1.save()
        
        section2 = Section({
            "name": "cmsc157-02",
            "courseNumber": "cmsc157",
            "schedule": "tth",
            "totalSeats": 10,
            "room": "ryerson 210",
            "isLab": False,
        })
        section2.save()
        
        reg = AdminController.addCourse(reg, course1)
        reg = AdminController.addSection(reg, section1)
        reg = AdminController.addSection(reg, section2)

        reg = RegistrationSession.fetch(reg.name)
        reg = RegistrationSession.fetch(reg.name)
        self.assertTrue(section1.name in reg.sections)
        self.assertTrue(section2.name in reg.sections)

    def test_removeAvailableSection(self):
        regParams = {
                "name": "winter 2022",
                "startDate": "01-2019",
                "endDate": "04-2019",
                "isOpen": 0,
                }

        reg = RegistrationSession(regParams)
        reg.save()

        course1 = Course({
                "courseNumber": "cmsc157",
                "division": "computer science",
                "subject": "OOP",
                "description": "object-oriented programming",
                "consentRequired": False,
                })
        course1.save()
        section1 = Section({
            "name": "cmsc157-01",
            "courseNumber": "cmsc157",
            "schedule": "mwf",
            "totalSeats": 10,
            "room": "ryerson 210",
            "isLab": False,
        })
        section1.save()
        
        section2 = Section({
            "name": "cmsc157-02",
            "courseNumber": "cmsc157",
            "schedule": "tth",
            "totalSeats": 10,
            "room": "ryerson 210",
            "isLab": False,
        })
        section2.save()

        reg = AdminController.addCourse(reg, course1)
        reg = AdminController.addSection(reg, section1)
        reg = AdminController.addSection(reg, section2)
        self.assertTrue(section1.name in reg.sections)
        self.assertTrue(section2.name in reg.sections)
        self.assertTrue(course1.courseNumber in reg.courses)

        reg = RegistrationSession.fetch(reg.name)
        reg = AdminController.removeSection(reg, 'cmsc157-01')
        self.assertTrue(section1.name not in reg.sections)
        self.assertTrue(section2.name in reg.sections)
        self.assertTrue(course1.courseNumber in reg.courses)

        reg = RegistrationSession.fetch(reg.name)
        reg = AdminController.removeSection(reg, 'cmsc157-02')
        self.assertTrue(section1.name not in reg.sections)
        self.assertTrue(section2.name not in reg.sections)
        self.assertTrue(course1.courseNumber in reg.courses)


if __name__ == '__main__':
    unittest.main()

