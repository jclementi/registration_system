import unittest
from unittest.mock import MagicMock

import regis.regiSys
from regis.regiSys import *
import regis.regisDb
from regis.regisDb import RegisDb

class TestRegistration(unittest.TestCase):

    regSessionParams = {
            "name": "autumn 2021",
            "startDate": "09-2021",
            "endDate": "12-2021",
            "isOpen": False,
    }

    studentParams = {
            "name": "shannon",
            "status": "full-time",
            "graduationDate": "June 2018",
    }

    courseParams01 = {
            "courseNumber": "cmsc158",
            "division": "computer science",
            "subject": "OOP",
            "description": "object-oriented programming",
            "consentRequired": False,
    }

    courseParams02 = {
            "courseNumber": "cmsc159",
            "division": "computer science",
            "subject": "OOP-er",
            "description": "more object-oriented programming",
            "consentRequired": False,
    }
    
    courseParams03 = {
            "courseNumber": "cmsc160",
            "division": "computer science",
            "subject": "OOP-est",
            "description": "still more object-oriented programming",
            "consentRequired": False,
    }

    courseParams04 = {
            "courseNumber": "cmsc999",
            "division": "computer science",
            "subject": "functional programming",
            "description": "jk this is not taught here",
            "consentRequired": False,
    }

    courseParams05 = {
            "courseNumber": "cmsc998",
            "division": "computer science",
            "subject": "secret programming",
            "description": "it is a secret to everyone",
            "consentRequired": True,
    }

    sectionParams01 = {
            "name": "cmsc158-01",
            "courseNumber": "cmsc158",
            "registrationSession": "autumn 2021",
            "schedule": "mwf",
            "totalSeats": 10,
            "room": "pick 305",
            "isLab": False,
    }

    sectionParams02 = {
            "name": "cmsc159-01",
            "courseNumber": "cmsc159",
            "registrationSession": "autumn 2021",
            "schedule": "mwf",
            "totalSeats": 10,
            "room": "ryerson 150",
            "isLab": False,
    }

    sectionParams03 = {
            "name": "cmsc160-01",
            "courseNumber": "cmsc160",
            "registrationSession": "autumn 2021",
            "schedule": "mwf",
            "totalSeats": 10,
            "room": "ryerson 150",
            "isLab": False,
    }

    sectionParams04 = {
            "name": "cmsc999-01",
            "courseNumber": "cmsc999",
            "registrationSession": "autumn 2021",
            "schedule": "mwf",
            "totalSeats": 10,
            "room": "ryerson 156",
            "isLab": False,
    }

    cparams = [ 
            courseParams01,
            courseParams02,
            courseParams03,
            courseParams04,
            ]

    sparams = [
            sectionParams01,
            sectionParams02,
            sectionParams03,
            sectionParams04,
            ]

    def setUp(self):
        db = RegisDb()
        db.initializeDatabase()
        # for c in TestRegistration.cparams:
        #     CourseController.add(c)
        # for s in TestRegistration.sparams:
        #     SectionController.add(s)

    def test_canAddCourseDuringOpenRegistration(self):
        session = RegistrationSession(TestRegistration.regSessionParams)
        session.save()
        course = Course(TestRegistration.courseParams01)
        course.save()
        section = Section(TestRegistration.sectionParams01)
        section.save()
        student = Student(TestRegistration.studentParams)
        student.save()

        AdminController.addCourse(session, course)
        AdminController.addSection(session, section)

        session.close()
        success, info = RegController.regStudent(section, student)
        self.assertFalse(success)
        self.assertEqual(info['desc'], "registration session is closed")

        session.open()
        success, info = RegController.regStudent(section, student)

        self.assertTrue(success)
        self.assertTrue(info is not None)
        self.assertEqual(info.section, "cmsc158-01")
        self.assertEqual(info.student, "shannon")


    def test_exceedMaxEnrollment(self):
        student = Student(TestRegistration.studentParams)
        student.save()

        reg = RegistrationSession(TestRegistration.regSessionParams)
        reg.save()
        reg.open()
        
        for c in TestRegistration.cparams:
            newCourse = Course(c)
            newCourse.save()
        for s in TestRegistration.sparams:
            newSection = Section(s)
            newSection.save()

        # enroll student in max courses
        RegController.regStudent('cmsc158-01', student)
        RegController.regStudent('cmsc159-01', student)
        RegController.regStudent('cmsc160-01', student)

        # enroll student in one more
        success, regRecord = RegController.regStudent('cmsc999-01', student)
        self.assertTrue(success)
        self.assertEqual(regRecord.status, "tentative")

    def test_consentRequiredEnrollment(self):
        student = Student(TestRegistration.studentParams)
        student.save()

        reg = RegistrationSession(TestRegistration.regSessionParams)
        reg.save()
        reg.open()

        cparams = {
                "courseNumber": "cmsc998",
                "division": "computer science",
                "subject": "secret programming",
                "description": "it is a secret to everyone",
                "consentRequired": True,
        }
        course = Course(cparams)
        course.save()

        sparams = {
                "name": "cmsc998-01",
                "courseNumber": "cmsc998",
                "registrationSession": "autumn 2021",
                "schedule": "mwf",
                "totalSeats": 1,
                "room": "ryerson 150",
                "isLab": False,
        }
        section = Section(sparams)
        section.save()

        success, regRecord = RegController.regStudent(section, student)
        self.assertTrue(success is True)
        self.assertEqual(regRecord.status, 'pending')


    def test_notEnoughSpace(self):
        student0 = Student(TestRegistration.studentParams)
        student0.save()
        student1 = Student(TestRegistration.studentParams)
        student1.name = "svengoolie"
        student1.save()

        reg = RegistrationSession(TestRegistration.regSessionParams)
        reg.save()
        reg.open()

        cparams = {
                "courseNumber": "cmsc998",
                "division": "computer science",
                "subject": "secret programming",
                "description": "it is a secret to everyone",
                "consentRequired": True,
        }
        course = Course(cparams)
        course.save()

        sparams = {
                "name": "cmsc998-01",
                "courseNumber": "cmsc998",
                "registrationSession": "autumn 2021",
                "schedule": "mwf",
                "totalSeats": 1,
                "room": "ryerson 150",
                "isLab": False,
        }
        section = Section(sparams)
        section.save()

        success, info = RegController.regStudent(section, student0)
        success, info = RegController.regStudent(section, student1)
        self.assertTrue(success is False)
        self.assertEqual(info['desc'], "there are no available seats")

    def test_prerequisiteEnrollment(self):
        student = Student(TestRegistration.studentParams)
        student.previousCourses = []
        student.save()

        reg = RegistrationSession(TestRegistration.regSessionParams)
        reg.save()
        reg.open()

        cparams = {
                "courseNumber": "cmsc987",
                "division": "computer science",
                "subject": "secret programming 1",
                "description": "it is a secret to everyone",
                "consentRequired": False,
        }
        course = Course(cparams)
        course.save()
        cparams = {
                "courseNumber": "cmsc988",
                "division": "computer science",
                "subject": "secret programming 2",
                "description": "it is a secret to everyone",
                "consentRequired": False,
        }
        course = Course(cparams)
        course.save()
        cparams = {
                "courseNumber": "cmsc989",
                "division": "computer science",
                "subject": "secret programming 3",
                "description": "it is a secret to everyone",
                "consentRequired": False,
                "prerequisiteCourses": ['cmsc988', 'cmsc987'],
        }
        course = Course(cparams)
        course.save()

        sparams = {
                "name": "cmsc989-01",
                "courseNumber": "cmsc989",
                "registrationSession": "autumn 2021",
                "schedule": "mwf",
                "totalSeats": 1,
                "room": "ryerson 150",
                "isLab": False,
        }
        section = Section(sparams)
        section.save()

        success, info = RegController.regStudent(section, student)
        self.assertTrue(success is False)
        self.assertEqual(info['desc'], "student has not taken prerequisite courses")


        student = Student(TestRegistration.studentParams)
        student.name = 'salamander'
        student.previousCourses = ['cmsc988', 'cmsc987']
        student.save()

        student = Student.fetch('salamander')
        success, info = RegController.regStudent(section, student)
        self.assertTrue(success)
        self.assertEqual(info.status, 'official')

    def test_labsRequired(self):
        student = Student(TestRegistration.studentParams)
        student.previousCourses = []
        student.save()

        reg = RegistrationSession(TestRegistration.regSessionParams)
        reg.save()
        reg.open()

        cparams = {
                "courseNumber": "cmsc987",
                "division": "computer science",
                "subject": "secret programming 1",
                "description": "it is a secret to everyone",
                "consentRequired": False,
        }
        course = Course(cparams)
        course.save()

        sparams = {
                "name": "cmsc987-01",
                "courseNumber": "cmsc987",
                "registrationSession": "autumn 2021",
                "schedule": "mwf",
                "totalSeats": 1,
                "room": "ryerson 150",
                "isLab": False,
        }
        section = Section(sparams)
        section.save()

        labparams = {
                "name": "cmsc987-01lab",
                "courseNumber": "cmsc987",
                "registrationSession": "autumn 2021",
                "schedule": "mwf",
                "totalSeats": 10,
                "room": "ryerson 151",
                "isLab": True,
        }
        section = Section(labparams)
        section.save()

        success, info = RegController.regStudent('cmsc987-01', student)
        self.assertFalse(success)
        self.assertEqual(info['desc'], "must register for lab sections first")

        RegController.regStudent('cmsc987-01lab', student)
        success, info = RegController.regStudent('cmsc987-01', student)
        self.assertTrue(success)
        self.assertEqual(info.status, 'official')

    def test_restrictionEnrollment(self):
        # I'm not implementing the administration system, so I'll have to 
        # pretend that holds exist and create them manually here.
        student = Student(TestRegistration.studentParams)
        student.previousCourses = []
        student.save()

        reg = RegistrationSession(TestRegistration.regSessionParams)
        reg.save()
        reg.open()

        cparams = {
                "courseNumber": "cmsc977",
                "division": "computer science",
                "subject": "secret programming 1",
                "description": "it is a secret to everyone",
                "consentRequired": False,
        }
        course = Course(cparams)
        course.save()

        sparams = {
                "name": "cmsc977-01",
                "courseNumber": "cmsc977",
                "registrationSession": "autumn 2021",
                "schedule": "mwf",
                "totalSeats": 1,
                "room": "ryerson 150",
                "isLab": False,
        }
        section = Section(sparams)
        section.save()

        # can't use a mock student because the method fetches a clean record
        # built from the database before running checks
        #
        # can't responsibly implement this test without admin holds in the 
        # database, so I'm leaving it out 
        # success, info = RegController.regStudent(section, student)
        # self.assertFalse(success)
        # self.assertEqual(info['desc'], "administrative holds prevent registration")
        pass

if __name__ == '__main__':
    unittest.main()
