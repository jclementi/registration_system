# System Description for New Course Registration System REGIE

## Final Project Requirements

You will find below the general requirements of a course registration system. In general, you will be required to model the requirements of the full system as described below. You will not, however, be required to individually code the full solution of these requirements. Rather, you will come to a solid understand of the requirements and then will write a proposal on what parts of the system described here you wish to deliver in code and tests, as an initial prototype of this system. More details on this will be provide in class and in Deliverable Six of Practicum I.

## General Description of the Problem Domain

The University of Chicago registrar's office has been relying on a course registration system written in the late 1990's based on an analysis of existing requirements at the time and implemented using the C language, with some C++. The design does not exist in any formal model but all the source code is available. The current system is implemented as a monolithic web application on a VAX 11/780 running BSD 4.3. The database backend is Oracle Server 10g. The data model is unavailable but one is in the process of being reverse-engineered. You are to develop a prototype for a new system that incorporates the existing system requirements as well as the new requirements, both described below.

## Current System Requirements

The current system handles logins for both faculty, students, and support staff. Faculty members and other instructors (including Professors, Associate Professors, Assistant Professors, Instructors, and Teaching Assistants, etc.) can log in under their own university ID with a password and can choose from a text-based menu which includes the following options:

1. View/print current and past course schedule and building/room assignments
2. View/print current and past class rosters of registered students
3. Assign grades for students in their course
4. Change already assigned grades
5. Change their password

Students (including both full and part-time students, TAs and students-at-large, etc.) can log in with a password and choose from a text-based menu which includes the following options:

1. Register for up to 3 courses per quarter
2. Add a course to their roster
3. Drop a course from their roster
4. Drop all courses
5. View/print courses on their roster (list courses for which they are registered)
6. View grades received for previous courses
7. Change their password
8. View restrictions

If a student attempts to register for a section of a course which is closed (filled the maximum number of students enrolled), the system will search for other sections of the course being offered and will present these alternatives to the user ranked by those at the closest date/time to the requested closed course. The student can alternatively choose to register for one of these alternate sections.

In order to register for courses, students need to know their student ID number, their system password, and the 8 digit subject/course/section number of the course for which they wish to register (every course section of course has a unique identifier). Students can also browse the entire course offering for the current quarter, choosing to browse by subject, subject and course, instructor, day, and day and time span. The current system does not do real-time checking of prerequisite coverage, but rather does a post facto batch examination and automatically drops the registration of courses for which the student has not met the prerequisites. The new system will do a real-time check that all prerequisites have been met before allowing the registration for the course to continue.

The new system being envisioned is a greatly-expanded version of the original system. It's target deployment will be on the Ubuntu Linux 10.4 operating system or above. Your prototype may be developed on any operating system you desire. The supporting software will include MySQL database and MongoDB, optionally with a web server such as the Apache Web Server.

The current system has a required availability of 20 hours per day, Monday - Saturday, from 6:00 am - 2:00 am. The system is down for consolidation and batch processing and conflict resolution from midnight until 6:00 am each day. The system is down for maintenance from midnight Saturday night until 6:00 am Monday morning.

## New System Requirements

The new system will support the following high-level system requirements.

The first stage is a functioning prototype (targeted release date of 4/1/2018) will support all student registration services of the legacy system. A separate team is doing the data model, and your design will be conducted irrespective of their efforts.

When a student successfully logs in to the registration system, any current courses for which he/she is registered will appear in a table form at the bottom of the screen. This display will be read-only and will only list the courses for which he/she is registered. To actually drop a course, the student should select the drop course command (the details of the GUI representation of this will be left until later).

Courses may require labs. A course may have any number of labs. For courses which require labs, the student must register for both a section of the course and an accompanying lab.

The new system will default to a limit of 3 courses, and will notify the student if he/she attempts to register for an overload (defined as more than 3 courses per quarter or as exceeding the student's personal course limit--which may be less than 3 courses in case of probation, etc.). The registration will be marked as pending, and an email will be generated to the department chair for approval of the overload, with a cc to the student's academic advisor and the student. Some courses may require consent of the instructor. In such cases, registration state is marked as "tentative" until an email has been generated to the appropriate instructor and the instructor has had an opportunity to review the course application and either approve the individual registration or not.

Once courses have been registered, a student can choose "schedule" to view a time sheet of his/her courses, presented in tabular form with the y-axis being time and the x-axis being the days of the week, Sunday - Saturday. Times for which classes have been scheduled appear highlighted on the spreadsheet with the course number (8 digits) and the course name and building in the selection.

Students can search for courses by division, course number, or instructor, as in the legacy system. When a course is displayed due to a user search, the current list of sections for the course will be displayed, along with the current real-time current registration information, including number of seats available in the section(s).

Students may make changes to their schedule by clicking on a scheduled class in the spreadsheet form, and choosing "modify registration" from the drop-down menu. From there, the student may make changes to the registration, including dropping the course, or rescheduling to another section/lab.

Students may be classified as either full-time or part-time. A part-time student cannot take more than 2 courses per quarter by default without override by the department chairperson. Full-time students are degree candidates and have an expected graduation date.

At any time, a student can print out a report of their schedule with the courses printed out on hard copy, in either report form or time sheet (spreadsheet) form.

At any time, a student can get a printout (to screen or printer) of their current transcript.

Instructors cannot register for courses for credit unless they are designated as Teaching Assistants, which can be either full- or part-time students in a given program. All full-time students are degree candidates and have either a major or concentration, as well as an assigned division and department.

There are several divisions, including the Division of Social Sciences, Division of Humanities, Division of Physical Sciences. Departments are assigned to divisions, for example, the Department of English Language and Literature is assigned to the Humanities Division. The department of Physics is assigned to the Physical Sciences Division. Faculty members are members of one or more departments (for example a given professor may be a professor in both the department of Physics as well as the department of Mathematics).

Faculty can log in and retrieve a listing of registered students in their courses. When faculty log in, they see a menu as well as a spreadsheet view of the course(s) they are teaching, presented in tabular form with the y-axis being time and the x-axis being the days of the week, Sunday - Saturday. Faculty can select a course and the dropdown menu will allow them to:

1. Send an email to all registered students
2. Request an online roster of currently registered students
3. Request a snail mail roster of registered students with photos
4. Approve/Deny requests for registration in Instructor Permission Required courses
5. Adjust modifiable course features such as Instructor's Approval Required, etc.
6. Go to the grade disposition spreadsheet

Faculty can report grades directly into the new system. After commanding the system to assign grades for a particular course/section, a spreadsheet appears with the names of all registered students (students auditing a course are grayed out and cannot be assigned a grade) and a grade column for the assignment of grades. Courses which are pass/fail will only allow a P or F grade, others will allow A, B, C, D, F, I, or blank assignments.

A course with fewer than 5 students is subject to cancellation at the discretion of the department chairperson.

The system will integrate with payroll and will allow for part-time employees to be paid on a per-course basis in addition to the default salary basis. All full-time and part-time faculty are employees, either full-time or part-time. Part-time employees are paid on a per-course basis. Full-time or part-time status has nothing to do with one's rank--that is, a part-time professor can be a full professor, or an instructor can be a full-time employee.

Maintenance of the system involves logging in to the system as an administrator. The administrator can:

1. Add new courses to the current set of course offerings (including dependent labs, etc.)
2. Schedule courses and sections
3. Delete courses for which there are fewer than 5 students or for which an instructor is unavailable.
4. Add/delete/modify students in the system
5. Add/delete/modify instructors (including professors/instructors of all ranks) in the system
6. Close registration for a course or set of courses (including "all")
7. Maintain building/time resources for the system

Courses in the system may have several sections. Each section will have a building and time period assigned. Each section may have one or more labs assigned. A lab will generally also have a building and time period assigned. Each section will have a minimum number of students enrollable (default of 5) and a maximum enrollment. A section may have one or more instructors assigned to it. A section may have one or more TAs and graders assigned to it (and some of these may also be students in the university), and of course each section has a certain number of students who are registered in the section. Each course has a brief description associated with it as well. The maximum number of students allowed in a section is dependent (non-overridable) on the building classroom selected. Each classroom has a maximum number of students allowed.

Rooms in buildings may be assigned to sections. Rooms will have characteristics including capacity, whiteboard, etc. A separate analytics program will run through available rooms and quarterly requirements of sections and will attempt a first-pass distribution of rooms to sections (you can simply use the "max enrollment/section" figure as a size requirement).

The system as a whole has a scheduled availability of 24/7.
