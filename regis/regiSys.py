from regis.database.dbCourses import DbCourses
from regis.database.dbDivisions import DbDivisions
from regis.database.dbMajors import DbMajors
from regis.database.dbInstructors import DbInstructors
from regis.database.dbStudents import DbStudents
from regis.database.dbSections import DbSections
from regis.database.dbRegistrationSess import DbRegistrationSess
from regis.database.dbMajors import DbMajors


class DbObject:
    @classmethod
    def fetch(cls, name):
        params = cls.db().select(name) 
        if params == None:
            return None
        return cls(params)

    @classmethod
    def fetchAll(cls):
        results = cls.db().selectAll()
        objects = list(map(lambda x: cls(x), results))
        return objects

    def save(self):
        params = self.fetch(getattr(self, self.nameAttr))
        if params == None:
            params = self.db.insert(self)

        return self.__class__.fetch(getattr(self, self.nameAttr))

    def modify(self, params):
        initRecord = self.fetch(getattr(self, self.nameAttr))
        rowId = initRecord.id

        for key in params:
            setattr(self, key, params[key])

        return self.db.update(rowId, params)

    def delete(self):
        name = getattr(self, self.__class__.nameAttr)
        self.db.delete(name)
        return None


class Validator:
    @classmethod
    def validate(cls, params):
        results = {}
        spec = cls.typeSpec
        inputIsValid = True

        for key, entry in spec.items():
            t, kind = entry
            valid = True

            if key not in params.keys():
                continue

            val = params[key]

            if kind == "list":
                if type(val) is not list:
                    valid = False
                else:
                    for i in val:
                        valid = (valid and isinstance(i, t))
            elif kind == "atom":
                if isinstance(val, t):
                    valid = True
                else:
                    valid = False
            else:
                valid = False

            results[key] = (t, valid)
            inputIsValid = (inputIsValid and valid)

        invalidParams = []

        for key, (t, valid) in results.items():
            if not valid:
                invalidParams.append(key)

        return inputIsValid, invalidParams


class Course(DbObject, Validator):
    db = DbCourses
    nameAttr = 'courseNumber'
    typeSpec = {
            "courseNumber": (str, "atom"),
            "division": (str, "atom"),
            "subject": (str, "atom"),
            "description": (str, "atom"),
            "consentRequired": (bool, "atom"),
            "prerequisiteCourses": (str, "list"),
    }


    def __init__(self, params = {}):
        self.db = DbCourses()
        self.courseNumber = 0
        self.division = ""
        self.subject = ""
        self.description = ""
        self.consentRequired = False
        self.prerequisiteCourses = []

        for key in params:
            setattr(self, key, params[key])


class Section(DbObject, Validator):
    db = DbSections
    nameAttr = 'name'
    typeSpec = {
            "name": (str, "atom"),
            "registrationSession": (str, "atom"),
            "courseNumber": (str, "atom"),
            "schedule": (str, "atom"),
            "totalSeats": (int, "atom"),
            "room": (str, "atom"),
            "isLab": (bool, "atom"),
            "instructors": (str, "list"),
            "students": (tuple, "list"),
    }

    def __init__(self, params = {}):
        self.db = DbSections()
        self.name = ""
        self.registrationSession = ""
        self.courseNumber = ""
        self.schedule = ""
        self.totalSeats = 0
        self.room = ""
        self.isLab = False
        self.instructors = []
        self.students = []

        for key in params:
            setattr(self, key, params[key])

    def availableSeats(self):
        occupied = len(self.students)
        return max(0, self.totalSeats - occupied)
    
    def allCourseSections(self):
        results = self.db.selectCourseSections(self.courseNumber)

        sections = list()
        if results is not None:
            for r in results:
                sections.append(Section(r))

        return sections


class RegistrationSession(DbObject, Validator):
    db = DbRegistrationSess
    nameAttr = 'name'
    typeSpec = {
            "name": (str, "atom"),
            "startDate": (str, 'atom'),
            'endDate': (str, 'atom'),
            'courses': (str, 'list'),
            'sections': (str, 'list'),
            'isOpen': (bool, 'atom'),
    }

    def __init__(self, params = {}):
        self.db = DbRegistrationSess()
        self.name = ""
        self.startDate = ""
        self.endDate = ""
        self.courses = []
        self.sections = []
        self.isOpen = False

        for key in params:
            setattr(self, key, params[key])

    def registrations(self, studentName):
        db = RegisDb()
        rs = db.fetchRegistrations(self.name)
        frs = [d for d in rs if d['student'] == studentName]
        secs = [r['section'] for r in frs]
        return secs

    def open(self):
        self.modify({"isOpen": True})

    def close(self):
        self.modify({"isOpen": False})


class Student(DbObject, Validator):
    db = DbStudents
    nameAttr = 'name'
    typeSpec = {
            "name": (str, "atom"),
            "status": (str, "atom"),
            "graduationDate": (str, "atom"),
            "sections": (tuple, "list"),
            "majors": (str, "list"),
            "previousCourses": (str, "list"),
    }

    def __init__(self, params = {}):
        self.db = DbStudents()
        self.name = ""
        self.status = ""
        self.graduationDate = ""
        self.sections = []
        self.majors = []
        self.previousCourses = []

        for key in params:
            setattr(self, key, params[key])

    def maxCourses(self):
        # determining other maxes requires admin system
        return 3

    def administrativeHolds(self):
        # call to admin system to get administration holds
        return []


class Instructor(DbObject, Validator):
    db = DbInstructors
    nameAttr = 'name'
    typeSpec = {
            "name": (str, "atom"),
            "divisions": (str, "list"),
            "sections": (str, "list"),
    }

    def __init__(self, params):
        self.db = DbInstructors()
        self.name = ""
        self.divisions = []
        self.sections = []

        for key in params:
            setattr(self, key, params[key])


class Major(DbObject, Validator):
    db = DbMajors
    nameAttr = 'name'
    typeSpec = {
            "name": (str, 'atom')
    }

    def __init__(self, params):
        self.db = DbMajors()
        self.name = ""

        for key in params:
            setattr(self, key, params[key])


class Division(DbObject, Validator):
    db = DbDivisions
    nameAttr = 'name'
    typeSpec = {
            "name": (str, 'atom')
    }

    def __init__(self, params):
        self.db = DbDivisions()
        self.name = ""

        for key in params:
            setattr(self, key, params[key])


# links students to sections
class Registration:
    def __init__(self, params):
        self.registrationSession = ""
        self.section = "",
        self.student =  "",
        self.status = "",

        for key in params:
            setattr(self, key, params[key])

# handles logic for registering students

class RegController:
    def notifyAdmin():
        # print("notifying admin")
        pass

    def unregStudent(section, student):
        regdsects = student.sections
        newsects = list()
        for secName, status in regdsects:
            if secName == section.name:
                continue
            newsects.append((secName, status))
        student.modify({'sections': newsects})
        return Student.fetch(student.name)

    def regStudent(section, student):
        # make sure the copies of the records we have are current
        if type(section) == str:
            section = Section.fetch(section)
        else:
            section = Section.fetch(section.name)

        if type(student) == str:
            student = Student.fetch(student)
        else:
            student = Student.fetch(student.name)

        session = RegistrationSession.fetch(section.registrationSession)
        course = Course.fetch(section.courseNumber)

        regParams = ({
            "registrationSession": session.name,
            "student": student.name,
            "section": section.name,
            "status": "official",
        })
        regRecord = Registration(regParams)

        # make sure registration is okay

        # session must be open
        if not session.isOpen:
            return (False, {"desc": "registration session is closed"})

        # section must have available seats
        if section.availableSeats() < 1:
            return (False, {"desc": "there are no available seats"})

        #student must have satisfied prerequisites
        for p in course.prerequisiteCourses:
            if p not in student.previousCourses:
                return (False, {"desc": "student has not taken prerequisite courses"})

        # if consent is required, enrollment is 'pending'
        if course.consentRequired:
            RegController.notifyAdmin()
            regRecord.status = 'pending'

        # student must not be enrolled in max number of courses
        if len(student.sections) >= student.maxCourses():
            RegController.notifyAdmin()
            regRecord.status = 'tentative'

        # if there are labs, student must be registered for labs first
        # query section db connector for all labs with the same course?
        # find the labs
        # see if student is registered for them
        if not section.isLab:
            allSections = section.allCourseSections()
            labSections = list()
            for s in allSections:
                if s.isLab == False:
                    continue

                labSections.append(s.name) 

            if len(labSections) > 0:
                # make sure student is registered for one of these
                regdSections = list()
                for tup in student.sections:
                    courseNumber, status = tup
                    regdSections.append(courseNumber)

                r = set(labSections).intersection(set(regdSections))
                if len(r) == 0:
                    return (False, {"desc": "must register for lab sections first"})

        # check for administrative holds
        
        if len(student.administrativeHolds()) > 0:
            return (False, {"desc": "administrative holds prevent registration"})

        # perform registration
        regdstudents = section.students
        regdstudents.append((student.name, regRecord.status))
        section.modify({'students': regdstudents})

        return (True, regRecord)


class AdminController:

    def addCourse(session, course):
        if type(session) == str:
            session = RegistrationSession.fetch(session)

        if type(course) == str:
            course = Course.fetch(course)

        currentCourses = session.courses
        currentCourses.append(course.courseNumber)

        session.modify({"courses": currentCourses})
        return RegistrationSession.fetch(session.name)

    def removeCourse(session, course):
        # removes sections connected to that course
        if type(session) == str:
            session = RegistrationSession.fetch(session)

        if type(course) == str:
            course = Course.fetch(course)

        currentCourses = session.courses
        currentCourses.remove(course.courseNumber)

        currentSections = session.sections
        ss = filter(lambda x: x.course in currentCourses)

        session.modify({
            "courses": currentCourses,
            "sections": list(ss),
        })
        return RegistrationSession.fetch(session.name)

    def addSection(session, section):
        if type(session) == str:
            session = RegistrationSession.fetch(session)

        if type(section) == str:
            section = Section.fetch(section)

        section.modify({"registrationSession": session.name})

        return RegistrationSession.fetch(session.name)

    def removeSection(session, section):
        if type(session) == str:
            session = RegistrationSession.fetch(session)

        if type(section) == str:
            section = Section.fetch(section)

        params = section.modify({"registrationSession": ""})
        newSec = Section.fetch(params['name'])
        newSes = RegistrationSession.fetch(session.name)
        return newSes

