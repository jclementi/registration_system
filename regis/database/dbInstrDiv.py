from regis.database.dbConnector import DbConnector
import regis.database

class DbInstrDiv(DbConnector):
    table = "instructorDivisions"
    tableCols = [
            'instructor_fk',
            'division_fk',
    ]
    tableDef = (
        "create table instructorDivisions ("
        "   `id`    int(11)     not null    auto_increment,"
        "   `instructor_fk` int(11),"
        "   `division_fk`    int(11),"
        "primary key (`id`)"
        ") engine=InnoDB")

    def resultToParams(self, tup):
        dbi = regis.database.dbInstructors.DbInstructors()
        instructor = dbi.getName(tup[1])
        dbd = regis.database.dbDivisions.DbDivisions()
        division = dbd.getName(tup[2])

        return {
                "id": tup[0],
                "instructor": instructor,
                "division": division,
                }

        
    # insert methods

    def resolveFks(self, assignments):
        dbi = regis.database.dbInstructors.DbInstructors()
        i = dbi.selectByName(assignments['instructor'])
        dbd = regis.database.dbDivisions.DbDivisions()
        d = dbd.selectByName(assignments['division'])

        insDict = {
                "instructor_fk": i['id'],
                "division_fk": d['id'],
                }

        return insDict
