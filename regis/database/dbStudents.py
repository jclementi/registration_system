from regis.database.dbConnector import DbConnector
from regis.database.dbRegistrations import DbRegistrations
from regis.database.dbDeclaredMajors import DbDeclaredMajors
from regis.database.dbPrevCourses import DbPrevCourses

class DbStudents(DbConnector):
    initialData = list()
    initialData.append({
        "name": "sam",
        "status": "full-time",
        "graduationDate": "01-06-2018",
        "majors": ['math'],
        })
    initialData.append({
        "name": "sacha",
        "status": "part-time",
        "graduationDate": "01-04-2018",
        "sections": ['cmsc150-01', 'cmsc151-01', 'phys151-01'],
        "majors": ['physics'],
        "previousCourses": ['cmsc150', 'phys150'],
        })

    table = "students"
    nameAttr = "name"
    tableCols = [
            'name',
            'status',
            'graduationDate',
    ]
    tableDef = (
        "create table students ("
        "   `id`    int(11)     not null auto_increment,"
        "   `name`  varchar(128),"
        "   `status`   varchar(128),"
        "   `graduationDate`    varchar(128),"
        "primary key (`id`)"
        ") engine=InnoDB")

    linkTableSpec = [{
        'key': 'majors',
        'connector': DbDeclaredMajors,
        'recordCol': 'student',
        'keyCol': 'major',
    },{
        'key': 'previousCourses',
        'connector': DbPrevCourses,
        'recordCol': 'student',
        'keyCol': 'courseNumber',
    }]

    def __init__(self):
        super().__init__()
        self.smjdb = DbDeclaredMajors()
        self.regdb = DbRegistrations()
        self.crsdb = DbPrevCourses()

    def resultToParams(self, tup):
        return {
                "id": tup[0],
                "name": tup[1],
                "status": tup[2],
                "graduationDate": tup[3],
                }


    # select methods

    def selectAssociations(self, student):
        student['sections'] = list()
        rs = self.regdb.selectByKey('student_fk', student['id'])
        if rs != None:
            for r in rs:
                student['sections'].append((r['section'], r['status']))

        student['majors'] = list()
        rs = self.smjdb.selectByKey('student_fk', student['id'])
        if rs != None:
            for r in rs:
                student['majors'].append(r['major'])

        student['previousCourses'] = list()
        rs = self.crsdb.selectByKey('student_fk', student['id'])
        if rs != None:
            for r in rs:
                student['previousCourses'].append(r['courseNumber'])

        return student


    # delete methods

    def deleteAssociations(self, s):
        self.regdb.deleteByKey('student_fk', s['id'])
        self.crsdb.deleteByKey('student_fk', s['id'])
        self.smjdb.deleteByKey('student_fk', s['id'])

    # requires special inserts for registrations, which require a status
    def insertLinkTables(self, assignments):
        sections = list()
        if 'sections' in assignments.keys():
            sections = assignments['sections']

        for s in sections:
            # get sections passed only as names
            if type(s) == str:
                a = {
                    'student': assignments['name'],
                    'section': s,
                    'status': 'official',
                    }
                self.regdb.insert(a)
                continue

            # get sections passed as (name, status) tuples
            section, status = s
            a = {
                'student': assignments['name'],
                'section': section,
                'status': status,
                }
            self.regdb.insert(a)

        super().insertLinkTables(assignments)
        return assignments
