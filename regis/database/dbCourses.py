from regis.database.dbConnector import DbConnector
from regis.database.dbDivisions import DbDivisions
from regis.database.dbPrereqs import DbPrereqs
from regis.database.dbAvailableCourses import DbAvailableCourses

class DbCourses(DbConnector):
    initialData = list()
    initialData.append({
        "courseNumber": "cmsc150",
        "subject": "pre-intro to programming",
        "description": "succeeds with basic registration",
        "consentRequired": 0,
        "division": "computer science",
        })
    initialData.append({
        "courseNumber": "cmsc151",
        "subject": "intro to programming",
        "description": "requires cmsc150 as prerequisite",
        "consentRequired": 0,
        "division": "computer science",
        "prerequisiteCourses": ['cmsc150'],
        })
    initialData.append({
        "courseNumber": "phys150",
        "subject": "intro to physics",
        "description": "requires instructor consent",
        "consentRequired": 1,
        "division": "physical science",
        })
    initialData.append({
        "courseNumber": "phys151",
        "subject": "intro to physics 2",
        "description": "requires instructor consent and phys151 prerequisite",
        "consentRequired": 1,
        "division": "physical science",
        "prerequisiteCourses": ["phys151"],
        })

    nameAttr = "courseNumber"
    table = "courses"
    tableCols = [
            'division_fk',
            'courseNumber',
            'subject',
            'description',
            'consentRequired',
    ]

    tableDef = (
       "create table courses ("
       "   `id`    int(11)     not null auto_increment,"
       "   `division_fk`   int(11),"
       "   `courseNumber`  varchar(16),"
       "   `subject`       varchar(128),"
       "   `description`   varchar(512),"
       "   `consentRequired`   int(1),"
       "primary key (`id`)"
       ") engine=InnoDB")

    linkTableSpec = [{
        'key': 'prerequisiteCourses',
        'connector': DbPrereqs,
        'recordCol': 'courseNumber',
        'keyCol': 'prereqNumber',
    }]

    def __init__(self):
        super().__init__()
        self.divdb = DbDivisions()
        self.prqdb = DbPrereqs()
        self.avldb = DbAvailableCourses()


    # select methods

    def selectAssociations(self, course):
        course['prerequisiteCourses'] = list()
        rs = self.prqdb.selectByKey('course_fk', course['id'])
        if rs != None:
            for r in rs:
                course['prerequisiteCourses'].append(r['prereqNumber'])

        return course


    # insert methods

    def resolveFks(self, assignments):
        # resolve foreign keys
        if 'division' in assignments.keys():
            divName = assignments['division']
            r = self.divdb.selectByKey("name", divName)
            division = r.pop()
            assignments['division_fk'] = division['id']

        return assignments


    # delete methods

    def deleteAssociations(self, c):
        self.prqdb.deleteByKey('course_fk', c['id'])
        self.avldb.deleteByKey('course_fk', c['id'])


    # helper methods

    def resultToParams(self, tup):
        # get names of foreign key items
        divName = self.divdb.getName(tup[1])
        consentRequired = tup[5]
        if consentRequired == 1:
            consentRequired = True
        else:
            consentRequired = False

        return {
                "id": tup[0],
                "courseNumber": tup[2],
                "division": divName,
                "subject": tup[3],
                "description": tup[4],
                "consentRequired": consentRequired,
                }
