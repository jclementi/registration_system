from regis.database.dbConnector import DbConnector
from regis.database.dbInstrDiv import DbInstrDiv
from regis.database.dbInstrAssign import DbInstrAssign


class DbInstructors(DbConnector):
    initialData = list()
    initialData.append({
        "name": "hector",
        "divisions": ['computer science'],
        "sections": ['cmsc150-01', 'cmsc150-02', 'cmsc151-01'],
        })
    initialData.append({
        "name": "helen",
        "divisions": ['humanities'],
        })

    nameAttr = "name"
    table = "instructors"
    tableCols = [
            'name',
    ]
    tableDef = (
        "create table instructors ("
        "   `id`    int(11)     not null    auto_increment,"
        "   `name`  varchar(128),"
        "primary key (`id`)"
        ") engine=InnoDB")

    linkTableSpec = [{
        'key': 'sections',
        'connector': DbInstrAssign,
        'recordCol': 'instructor',
        'keyCol': 'sectionName',
    }, {
        'key': 'divisions',
        'connector': DbInstrDiv,
        'recordCol': 'instructor',
        'keyCol': 'division',
    }]

    def __init__(self):
        super().__init__()
        self.iadb = DbInstrAssign()
        self.divdb = DbInstrDiv()

    def resultToParams(self, tup):
        return {"id": tup[0], "name": tup[1]}


    # select methods

    def selectAssociations(self, i):
        # stitch on the linking table info
        i['sections'] = list()
        rs = self.iadb.selectByKey('instructor_fk', i['id'])
        if rs != None:
            for r in rs:
                i['sections'].append(r['sectionName'])

        i['divisions'] = list()
        divs = self.divdb.selectByKey('instructor_fk', i['id'])
        if divs != None:
            for d in divs:
                i['divisions'].append(d['division'])

        return i


    # delete methods

    def deleteAssociations(self, i):
        self.iadb.deleteByKey('instructor_fk', i['id'])
        self.divdb.deleteByKey('instructor_fk', i['id'])
