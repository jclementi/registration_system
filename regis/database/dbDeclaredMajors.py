from regis.database.dbConnector import DbConnector
import regis.database

class DbDeclaredMajors(DbConnector):
    table = "declaredMajors"
    tableCols = [
            'student_fk',
            'major_fk',
    ]
    tableDef = (
        "create table declaredMajors ("
        "   `id`    int(11)     not null    auto_increment,"
        "   `student_fk`    int(11),"
        "   `major_fk`  int(11),"
        "primary key (`id`)"
        ") engine=InnoDB")

    def resultToParams(self, tup):
        dbmaj = regis.database.dbMajors.DbMajors()
        dbstu = regis.database.dbStudents.DbStudents()

        student = dbstu.getName(tup[1])
        major = dbmaj.getName(tup[2])

        return {
                "id": tup[0],
                "student": student,
                "major": major,
        }

    # insert methods

    def resolveFks(self, assignments):
        dbmaj = regis.database.dbMajors.DbMajors()
        major = dbmaj.selectByName(assignments['major'])
        dbstu = regis.database.dbStudents.DbStudents()
        student = dbstu.selectByName(assignments['student'])

        assignments['student_fk'] = student['id']
        assignments['major_fk'] = major['id']

        return assignments

        
