from regis.database.dbConnector import DbConnector
from regis.database.dbCourses import DbCourses
from regis.database.dbRegistrationSess import DbRegistrationSess
from regis.database.dbRegistrations import DbRegistrations
from regis.database.dbInstrAssign import DbInstrAssign

class DbSections(DbConnector):
    initialData = list()
    initialData.append({
        "name": "cmsc150-01",
        "registrationSession": "autumn 2018",
        "schedule": "mwf",
        "totalSeats": 10,
        "room": "kent 102",
        "isLab": 0,
        "courseNumber": "cmsc150",
        })
    initialData.append({
        "name": "cmsc150-02",
        "registrationSession": "autumn 2018",
        "schedule": "tth",
        "totalSeats": 1,
        "room": "kent 103",
        "isLab": 0,
        "courseNumber": "cmsc150",
        })
    initialData.append({
        "name": "phys150-01",
        "registrationSession": "autumn 2018",
        "schedule": "tth",
        "totalSeats": 40,
        "room": "kent 150",
        "isLab": 0,
        "courseNumber": "phys150",
        })
    initialData.append({
        "name": "phys150-02",
        "registrationSession": "autumn 2018",
        "schedule": "mwf",
        "totalSeats": 40,
        "room": "kent 150",
        "isLab": 0,
        "courseNumber": "phys150",
        })
    initialData.append({
        "name": "phys151-01",
        "registrationSession": "autumn 2018",
        "schedule": "tth",
        "totalSeats": 40,
        "room": "kent 150",
        "isLab": 0,
        "courseNumber": "phys151",
        })
    initialData.append({
        "name": "cmsc151-01",
        "registrationSession": "autumn 2018",
        "schedule": "mwf",
        "totalSeats": 10,
        "room": "ryerson 151",
        "isLab": 0,
        "courseNumber": "cmsc151"
        })
    initialData.append({
        "name": "cmsc151-01lab",
        "registrationSession": "autumn 2018",
        "schedule": "tth",
        "totalSeats": 10,
        "room": "ryerson 151",
        "isLab": 1,
        "courseNumber": "cmsc151"
        })

    nameAttr = 'name'
    table = 'sections'
    tableDef = (
        "create table sections ("
        "   `id`    int(11)     not null auto_increment,"
        "   `course_fk`    int(11),"
        "   `registrationSession_fk` int(11),"
        "   `name`  varchar(128),"
        "   `schedule`  varchar(128),"
        "   `totalSeats`    int(8),"
        "   `room`  varchar(128),"
        "   `isLab` int(1),"
        "primary key (`id`)"
        ") engine=InnoDB")
    
    tableCols = [
            'course_fk',
            'registrationSession_fk',
            'name',
            'schedule',
            'totalSeats',
            'room',
            'isLab',
    ]

    linkTableSpec = [{
        'key': 'instructors',
        'connector': DbInstrAssign,
        'recordCol': 'sectionName',
        'keyCol': 'instructor',
    }]

    def __init__(self):
        super().__init__()
        self.crsdb = DbCourses()
        self.rssdb = DbRegistrationSess()
        self.regdb = DbRegistrations()
        self.iadb = DbInstrAssign()


    # insert methods
    
    def resolveFks(self, assignments):
        if 'registrationSession' in assignments.keys():
            regName = assignments['registrationSession']
            if regName == "":
                assignments['registrationSession_fk'] = None
            else:
                regses = self.rssdb.selectByName(regName)
                assignments['registrationSession_fk'] = regses['id']

        if 'courseNumber' in assignments.keys():
            cn = assignments['courseNumber']
            course = self.crsdb.selectByName(cn)
            if course == None:
                raise Exception('failed to find course {}'.format(cn))
            assignments['course_fk'] = course['id']

        return assignments


    # update methods

    def updateRecord(self, rowId, assignments):
        assignments = self.resolveFks(assignments)
        assignString = self._buildAssignString(assignments)
        if assignString == "":
            return self.selectById(rowId)

        u = "update {table} set\n{assignString} where id='{idval}'".format(
                table = self.table,
                assignString = assignString,
                idval = rowId,
                )

        self.commitQuery(u)
        params = self.selectById(rowId)
        return params
    

    # insert methods

    # requires special inserts for registrations, which require a status
    def insertLinkTables(self, assignments):
        students = list()
        if 'students' in assignments.keys():
            students = assignments['students']

        for s in students:
            # get sections passed only as names
            if type(s) == str:
                a = {
                    'section': assignments['name'],
                    'student': s,
                    'status': 'official',
                    }
                self.regdb.insert(a)
                continue

            # get sections passed as (name, status) tuples
            student, status = s
            a = {
                'section': assignments['name'],
                'student': student,
                'status': status,
                }
            self.regdb.insert(a)

        super().insertLinkTables(assignments)
        return assignments


    # select methods

    def selectAssociations(self, section):
        section['students'] = list()
        rs = self.regdb.selectByKey('section_fk', section['id'])
        if rs != None:
            for r in rs:
                section['students'].append((r['student'], r['status']))

        section['instructors'] = list()
        rs = self.iadb.selectByKey('section_fk', section['id'])
        if rs != None:
            for r in rs:
                section['instructors'].append(r['instructor'])

        return section
    
    def selectCourseSections(self, courseNumber):
        # if it's not a string, it's a course object
        if type(courseNumber) != str:
            courseNumber = courseNumber.courseNumber

        course_fk = self.crsdb.getFk(courseNumber)

        results = self.selectByKey('course_fk', course_fk)
        return results


    # delete methods

    def deleteAssociations(self, s):
        self.regdb.deleteByKey('section_fk', s['id'])
        self.iadb.deleteByKey('section_fk', s['id'])


    # helper methods

    def resultToParams(self, tup):
        courseNumber = self.crsdb.getName(tup[1])
        if tup[2] is not None:
            regname = self.rssdb.getName(tup[2])
        else:
            regname = ""

        islab = tup[7]
        if islab == 1:
            islab = True
        else: 
            islab = False

        return {
                "id": tup[0],
                "name": tup[3],
                "courseNumber": courseNumber,
                "registrationSession": regname,
                "schedule": tup[4],
                "totalSeats": tup[5],
                "room": tup[6],
                "isLab": islab,
                }

