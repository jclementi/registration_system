from regis.database.dbConnector import DbConnector

class DbDivisions(DbConnector):
    initialData = list()
    initialData.append({"name": "computer science"})
    initialData.append({"name": "physical science"})
    initialData.append({"name": "humanities"})

    nameAttr = "name"
    table = "divisions"
    tableCols = [
            'name',
    ]
    tableDef = (
        "create table divisions ("
        "   `id`    int(11)     not null    auto_increment,"
        "   `name`  varchar(128),"
        "primary key (`id`)"
        ") engine=InnoDB")

    def resultToParams(self, tup):
        return {"id": tup[0], "name": tup[1]}
