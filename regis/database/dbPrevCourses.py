from regis.database.dbConnector import DbConnector
import regis.database


class DbPrevCourses(DbConnector):
    table = "prevCourses"
    tableCols = [
            'student_fk',
            'course_fk',
    ]
    tableDef = (
        "create table prevCourses ("
        "   `id`    int(11)     not null    auto_increment,"
        "   `student_fk`   int(11),"
        "   `course_fk`    int(11),"
        "primary key (`id`)"
        ") engine=InnoDB")

    def resultToParams(self, tup):
        dbs = regis.database.dbStudents.DbStudents()
        dbc = regis.database.dbCourses.DbCourses()
        student = dbs.getName(tup[1])
        courseNumber = dbc.getName(tup[2])

        return {
                "id": tup[0],
                "student": student,
                "courseNumber": courseNumber,
                }

    def resolveFks(self, assignments):
        dbs = regis.database.dbStudents.DbStudents()
        sid = dbs.getFk(assignments['student'])

        dbc = regis.database.dbCourses.DbCourses()
        cid = dbc.getFk(assignments['courseNumber'])

        assignments['student_fk'] = sid
        assignments['course_fk'] = cid

        return assignments
