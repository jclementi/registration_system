from regis.database.dbConnector import DbConnector

class DbMajors(DbConnector):
    initialData = list()
    initialData.append({"name": "physics"})
    initialData.append({"name": "math"})
    initialData.append({"name": "humanities"})

    nameAttr = "name"
    table = "majors"
    tableCols = [
            'name',
    ]
    tableDef = (
        "create table majors ("
        "   `id`    int(11)     not null    auto_increment,"
        "   `name`  varchar(128),"
        "primary key (`id`)"
        ") engine=InnoDB")

    def resultToParams(self, tup):
        return {"id": tup[0], "name": tup[1]}
