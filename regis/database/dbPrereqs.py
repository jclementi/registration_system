from regis.database.dbConnector import DbConnector
import regis.database.dbCourses


class DbPrereqs(DbConnector):
    table = "prerequisites"
    tableCols = [
            'course_fk',
            'prereq_course_fk',
    ]
    tableDef = (
        "create table prerequisites ("
        "   `id`    int(11)     not null    auto_increment,"
        "   `course_fk`   int(11),"
        "   `prereq_course_fk`    int(11),"
        "primary key (`id`)"
        ") engine=InnoDB")

    def resultToParams(self, tup):
        dbc = regis.database.dbCourses.DbCourses()
        courseName = dbc.getName(tup[1])
        prereqName = dbc.getName(tup[2])

        return {
                "id": tup[0], 
                "courseNumber": courseName,
                "prereqNumber": prereqName,
                }
    

    # insert methods

    def resolveFks(self, assignments):
        dbc = regis.database.dbCourses.DbCourses()
        c = dbc.selectByName(assignments['courseNumber'])
        req = dbc.selectByName(assignments['prereqNumber'])

        insDict = {
                "course_fk": c['id'],
                "prereq_course_fk": req['id'],
                }

        return insDict
