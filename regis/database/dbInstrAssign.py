from regis.database.dbConnector import DbConnector
import regis.database.dbInstructors


class DbInstrAssign(DbConnector):
    table = "instructorAssignments"
    nameAttr = "instructor_fk"
    tableCols = [
            'instructor_fk',
            'section_fk',
    ]
    tableDef = (
        "create table instructorAssignments ("
        "   `id`    int(11)     not null    auto_increment,"
        "   `instructor_fk` int(11),"
        "   `section_fk`    int(11),"
        "primary key (`id`)"
        ") engine=InnoDB")

    def resultToParams(self, tup):
        dbi = regis.database.dbInstructors.DbInstructors()
        dbs = regis.database.dbSections.DbSections()
        instructor = dbi.getName(tup[1])
        sectionName = dbs.getName(tup[2])

        return {
                "id": tup[0], 
                "instructor": instructor,
                "sectionName": sectionName,
                }


    # insert methods

    def resolveFks(self, assignments):
        dbi = regis.database.dbInstructors.DbInstructors()
        i = dbi.selectByName(assignments['instructor'])

        dbs = regis.database.dbSections.DbSections()
        s = dbs.selectByName(assignments['sectionName'])

        assignments['instructor_fk'] = i['id']
        assignments['section_fk'] = s['id']

        return assignments

