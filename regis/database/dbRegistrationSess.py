from regis.database.dbConnector import DbConnector
from regis.database.dbAvailableCourses import DbAvailableCourses
import regis.database 


class DbRegistrationSess(DbConnector):
    initialData = list()
    initialData.append({
        "name": "spring 2018",
        "startDate": "01-02-2018",
        "endDate": "01-04-2018",
        "isOpen": 1,
        })
    initialData.append({
        "name": "autumn 2018",
        "startDate": "01-06-2018",
        "endDate": "01-08-2018",
        "isOpen": 1,
        })

    table = 'registrationSessions'
    nameAttr = 'name'
    tableCols = [
            'name',
            'startDate',
            'endDate',
            'isOpen',
    ]
    tableDef = (
        "create table registrationSessions ("
        "   `id`    int(11)     not null    auto_increment,"
        "   `name`  varchar(128),"
        "   `startDate` varchar(128),"
        "   `endDate`   varchar(128),"
        "   `isOpen`    int(1),"
        "primary key (`id`)"
        ") engine=InnoDB")

    linkTableSpec = [{
        'key': 'courses',
        'connector': DbAvailableCourses,
        'recordCol': 'registrationSession',
        'keyCol': 'courseNumber',
    }]

    def __init__(self):
        super().__init__()
        self.avcdb = DbAvailableCourses()
        # self.secdb = regis.database.dbSections.DbSections()

    def resultToParams(self, tup):
        isOpen = tup[4]
        if isOpen == 1:
            isOpen = True
        else:
            isOpen = False

        return {
                "id": tup[0],
                "name": tup[1],
                "startDate": tup[2],
                "endDate": tup[3],
                "isOpen": isOpen,
                }

    # select methods
    
    def selectAssociations(self, session):
        session['courses'] = list()
        rs = self.avcdb.selectByKey('registrationSession_fk', session['id'])
        if rs != None:
            for r in rs:
                session['courses'].append(r['courseNumber'])
        
        session['sections'] = list()
        avsdb = regis.database.dbSections.DbSections()
        rs = avsdb.selectByKey('registrationSession_fk', session['id'])
        if rs != None:
            for r in rs:
                session['sections'].append(r['name'])

        return session

            
    # delete methods

    def deleteAssociations(self, r):
        self.avcdb.deleteByKey('registrationSession_fk', r['id'])

