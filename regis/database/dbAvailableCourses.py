from regis.database.dbConnector import DbConnector
import regis.database

class DbAvailableCourses(DbConnector):
    table = "availableCourses"
    tableCols = [
            'course_fk',
            'registrationSession_fk',
    ]
    tableDef = (
        "create table availableCourses ("
        "   `id`    int(11)     not null    auto_increment,"
        "   `registrationSession_fk`    int(11),"
        "   `course_fk`     int(11),"
        "primary key (`id`)"
        ") engine=InnoDB")

    def resultToParams(self, tup):
        dbr = regis.database.dbRegistrationSess.DbRegistrationSess()
        dbc = regis.database.dbCourses.DbCourses()
        session = dbr.getName(tup[1])
        courseNumber = dbc.getName(tup[2])

        return {
                "id": tup[0],
                "registrationSession": session,
                "courseNumber": courseNumber,
                }

    # insert methods

    def resolveFks(self, assignments):
        dbr = regis.database.dbRegistrationSess.DbRegistrationSess()
        dbc = regis.database.dbCourses.DbCourses()
        session = dbr.selectByName(assignments['registrationSession'])
        course = dbc.selectByName(assignments['courseNumber'])

        assignments['registrationSession_fk'] = session['id']
        assignments['course_fk'] = course['id']

        return assignments
