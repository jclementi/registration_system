import re
import mysql.connector
from mysql.connector import errorcode

# abstract base class for database connectors for other objects
#
# expects subclasses to have attributes:
# * nameAttr - the unqiue name attribute in the database
# * table - name of the table
# * tableCols - the columns that should be fetched on select
# * tableDef - the table definition statement
# * initialData - a list of dictionaries that define initial data
# * linkTableSpec - a spec for how this table relates to linking tables
#  
# expects subclasses to implement
# * resultToParams
#

class DbConnector:

    db_name = 'regis_db'
    initialData = None
    linkTableSpec = []

    def __init__(self):
        try:
            db_connection = mysql.connector.connect(user='root', password='root')
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Acess denied/wrong user name or password")

        self.connection = db_connection
        self.cursor = db_connection.cursor()

        try:
            self.connection.database = DbConnector.db_name
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_BAD_DB_ERROR:
                self.createDatabase()
            else:
                print(err)

    def __del__(self):
        self.connection.close()
        self.cursor.close()

    # implemented by subclasses
    # returns a dictionary that can be used to instantiate host class
    def resultToParams(self, tup):
        pass


    # selects

    def select(self, name):
        record = self.selectByName(name)
        if record == None:
            return None

        # stitch on any linking table info
        r = self.selectAssociations(record)
        return r

    def selectAll(self):
        s = "select * from {table}".format(table = self.__class__.table)
        self.query(s)
        results = list()

        for row in self.cursor:
            rec = self.resultToParams(row)
            rec = self.selectAssociations(rec)
            results.append(rec)

        return results

    def selectAssociations(self, record):
        return record

    # retuns the id of the row with the given unique name
    def getFk(self, name):
        r = self.selectByName(name)
        if r == None:
            return None
        return r['id']

    def getName(self, rowId):
        r = self.selectById(rowId)
        if r == None:
            return None
        return r[self.__class__.nameAttr]

    # returns only one result
    def selectById(self, rowId):
        s = "select * from {table} where id = {rowId}".format(
                table = self.__class__.table,
                rowId = rowId)
        self.query(s)
        results = list()

        for row in self.cursor:
            results.append(self.resultToParams(row))

        if len(results) > 1:
            print("more than one result when looking up {} by id".format(self.__class__.__name__))
            print("results: ", results)
        if len(results) == 0:
            return None

        r = results.pop()
        return r

    # returns only one result
    def selectByName(self, name):
        results = self.selectByKey(self.__class__.nameAttr, name)
        if results == None:
            return None
        if len(results) > 1:
            print("more than one result when looking up {} by name".format(self.__class__.__name__))
            print("results: ", results)
        r = results.pop()
        return r

    # returns a list of results
    def selectByKey(self, key, name):
        s = "select * from {table} where {key} = '{name}'".format(
                table = self.__class__.table,
                key = key,
                name = name)
        self.query(s)
        results = list()
        for row in self.cursor:
            results.append(self.resultToParams(row))
        if len(results) == 0:
            return None
        return results

        
    # inserts have enough structure to allow DbConnector to define a reasonable default
    def insert(self, assignments):
        a = self.prepInsert(assignments)
        a = self.resolveFks(a)
        newRec = self.insertDict(a)
        self.insertLinkTables(a)
        return newRec

    def prepInsert(self, assignments):
        adict = assignments
        if type(adict) != dict:
            adict = vars(assignments)

        return adict

    def resolveFks(self, assignments):
        return assignments

    def insertDict(self, assignments):
        assignString = self._buildAssignString(assignments)

        i = 'insert into {table} set {assignString}'.format(
                table = self.__class__.table, 
                assignString = assignString)

        self.commitQuery(i)

        rowId = self.cursor.lastrowid
        params = self.selectById(rowId)

        return params 

    def insertLinkTables(self, assignments):
        for s in self.__class__.linkTableSpec:
            if s['key'] in assignments.keys():
                vals = assignments[s['key']]
                for v in vals:
                    a = {
                        s['recordCol']: assignments[self.__class__.nameAttr],
                        s['keyCol']: v,
                        }

                    s['connector']().insert(a)

        return assignments


    # updates have enough structure to allow DbConnector to define a reasonable default
    # name - value for the class nameAttr
    # assignments - dict with keys and values to update with
    def update(self, rowId, assignments):
        params = self.updateRecord(rowId, assignments)

        # updateLinkTables expects the class' unique name attribute to be set
        name = self.getName(rowId)
        assignments[self.__class__.nameAttr] = name
        self.updateLinkTables(rowId, assignments)
        return params

    # deletes all rows in linking tables, then creates new ones
    def updateLinkTables(self, rowId, recordDict):
        recordDict['id'] = rowId
        self.deleteAssociations(recordDict)
        updatedDict = self.insertLinkTables(recordDict)
        return updatedDict


    def updateRecord(self, rowId, assignments):
        assignments = self.resolveFks(assignments)
        assignString = self._buildAssignString(assignments)
        if assignString == "":
            return self.selectById(rowId)

        u = "update {table} set\n{assignString} where id='{idval}'".format(
                table = self.table,
                assignString = assignString,
                idval = rowId,
                )

        self.commitQuery(u)
        rowId = self.cursor.lastrowid
        params = self.selectById(rowId)
        return params
    

    # delete functions

    # assumes it gets the unique name
    def delete(self, name):
        record = self.selectByName(name)
        self.deleteAssociations(record)
        self.deleteByKey(self.__class__.nameAttr, name)
    
    def deleteByKey(self, key, val):
        d = "delete from {table} where {key}='{val}'".format(
                table = self.__class__.table,
                key = key,
                val = val)

        self.commitQuery(d)

    def deleteAssociations(self, delDict):
        pass


    # helper functions

    def _buildAssignString(self, assignments):
        a = list()
        for col in self.__class__.tableCols:

            if col not in assignments.keys():
                continue

            v = assignments[col]

            if type(v) == bool:

                if v == True:
                    a.append("{key}=1".format(key = col))
                elif v == False:
                    a.append("{key}=0".format(key = col))

            elif type(v) == int:
                a.append("{key}={val}".format(key = col, val = v))
            elif type(v) == str:
                a.append("{key}='{val}'".format(key = col, val = v))
            elif v == None:
                a.append("{key}=NULL".format(key = col))

        assignString = ",\n\t".join(a)
        return assignString


    def createTable(self):
        self.commitQuery(self.__class__.tableDef)

    def insertInitialData(self):
        idata = self.__class__.initialData
        if idata == None:
            return None
        for d in idata:
            self.insert(d)

    def commitQuery(self, query, *args):
        try:
            self.cursor.execute(query, *args)
        except mysql.connector.Error as err:
            print("error: ", err)
            print("statement: ", self.cursor.statement)

        self.connection.commit()

    def query(self, query, *args):
        if 'None' in query:
            raise Exception('"None" in query')
        try:
            self.cursor.execute(query, *args)
        except mysql.connector.Error as err:
            print("error: ", err)
            print("statement: ", self.cursor.statement)

