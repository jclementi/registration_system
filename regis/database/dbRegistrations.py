from regis.database.dbConnector import DbConnector
import regis.database


class DbRegistrations(DbConnector):
    table = "registrations"
    tableCols = [
            'student_fk',
            'section_fk',
            'status',
    ]
    tableDef = (
        "create table registrations ("
        "   `id`    int(11)     not null    auto_increment,"
        "   `student_fk`    int(11),"
        "   `section_fk`    int(11),"
        "   `status`    varchar(128),"
        "primary key (`id`)"
        ") engine=InnoDB")

    def resultToParams(self, tup):
        dbstu = regis.database.dbStudents.DbStudents()
        dbsec = regis.database.dbSections.DbSections()
        studentName = dbstu.getName(tup[1])
        secName = dbsec.getName(tup[2])

        return {
                "id": tup[0],
                "student": studentName,
                "section": secName,
                "status": tup[3],
        }

    # insert methods

    def insertDict(self, assignments):
        if 'status' not in assignments.keys():
            assignments['status'] = 'official'

        return super().insertDict(assignments)

    def resolveFks(self, assignments):
        dbstu = regis.database.dbStudents.DbStudents()
        dbsec = regis.database.dbSections.DbSections()
        student = dbstu.selectByName(assignments['student'])
        section = dbsec.selectByName(assignments['section'])

        assignments['student_fk'] = student['id']
        assignments['section_fk'] = section['id']

        return assignments
