# command-line interface for the regis system
import regis.regiSys
from regis.regiSys import *

def run():
    print("command-line interface for regis system")
    while True:
        i = loginMenu()
        if i == 'q':
            return


def loginMenu():
    students = Student.fetchAll()
    i, record = selectFromList(
            students,
            Student,
            "select the person you are; 'a' for admin; 'q' to quit: "
            )

    if i == 'a':
        adminMenu()
        return
    elif record is not None:
        studentMenu(record)
        return

    return i


def studentMenu(student):
    while True:
        studentHomeGreeting = (
                "\n\n\tStudent Home Screen: {}\n"
                ).format(student.name)
        
        print(studentHomeGreeting)
        regSessions = RegistrationSession.fetchAll()
        i, session = selectFromList(
                regSessions,
                RegistrationSession,
                "select a registration session to browse; 'q' to quit: ")

        if session is not None:
            registrationMenu(student, session)
        elif i == 'q':
            print('exiting student menu')
            return

            
def registrationMenu(student, session):
    # present current sections
    # present sections to add
    while True: 
        student = Student.fetch(student.name)
        session = RegistrationSession.fetch(session.name)
        ss = Section.fetchAll()
        ss = list(filter(lambda x: x.registrationSession == session.name, ss))
        # filter for student
        studss = list()
        for s in ss:
            regs = s.students
            for sname, status in regs:
                if sname == student.name:
                    studss.append(s)

        ss = studss

        print("registered sections for this session:")
        i, section = selectFromList(
                ss,
                Section,
                "select session to remove; 'a' to add; 'q' to quit")

        if i == 'q':
            return
        elif i == 'a':
            # only display sections with courses available in this session
            ss = Section.fetchAll()
            ss = list(filter(lambda x: x.registrationSession == session.name, ss))
            ss = list(filter(lambda x: x.name not in student.sections, ss))
            
            i, section = selectFromList(
                    ss,
                    Section,
                    "select section to register for: ")

            if section is not None:
                success, info = RegController.regStudent(section, student)
                if not success:
                    print("registration failed: {}".format(info['desc']))
                else:
                    print("registration succeeded") 
                return
        # remove
        elif section is not None:
           RegController.unregStudent(section, student) 


def adminMenu():
    while True:
        adminHomeGreeting = (
                "\n\n\tAdmin Home Screen\n"
                "\tSelect an option:\n\n"
                "Registration Session Administration\n"
                "1: edit courses for registration session\n"
                "2: edit sections for registration session\n\n"
                "Raw Record Editing\n"
                "m: major\n"
                "d: division\n"
                "c: course\n"
                "r: registration session\n"
                "s: student\n"
                "n: section\n"
                "i: instructor\n"
                "q: quit\n"
                "\n:> "
                )
        i = input(adminHomeGreeting)
        if i == 'q':
            print('exiting admin menu')
            return
        elif i == '1':
            editCoursesForReg()
        elif i == '2':
            editSectionsForReg()
        elif i == 'm':
            recordMenu(Major)
        elif i == 'd':
            recordMenu(Division)
        elif i == 'c':
            recordMenu(Course)
        elif i == 'r':
            recordMenu(RegistrationSession)
        elif i == 's':
            recordMenu(Student)
        elif i == 'n':
            recordMenu(Section)
        elif i == 'i':
            recordMenu(Instructor)


def editCoursesForReg():
    # select registration session
    records = RegistrationSession.fetchAll()
    i, session = selectFromList(
            records,
            RegistrationSession,
            "select session to edit courses for; 'q' to quit: ")
    if session == None:
        return
    session = RegistrationSession.fetch(session.name)

    allCourses = Course.fetchAll()
    currentCourses = list(filter(lambda x: x.courseNumber in session.courses, allCourses))
    i, course = selectFromList(
            currentCourses,
            Course,
            "select a course to remove; 'a' to add; 'q' to quit")

    if i == 'q':
        return None
    if course is not None:
        AdminController.removeCourse(session, course)
    elif i == 'a':
        # filter current courses
        cs = filter(lambda x: x.courseNumber not in session.courses, allCourses) 
        i, course = selectFromList(
                list(cs),
                Course, 
                "select course to add; 'q' to quit")
        if course == None:
            return
        else:
            AdminController.addCourse(session, course)
    else: 
        return

    printRecord(session, RegistrationSession)
    return


def editSectionsForReg():
    records = RegistrationSession.fetchAll()
    i, session = selectFromList(
            records,
            RegistrationSession,
            "select session to edit sections for; 'q' to quit: ")
    if session == None:
        return

    session = RegistrationSession.fetch(session.name)

    allSections = Section.fetchAll()
    currentSections = list(filter(lambda x: x.name in session.sections, allSections))
    i, section = selectFromList(
            currentSections,
            Section,
            "select a section to remove; 'a' to add; 'q' to quit")

    if i == 'q':
        return
    if section is not None:
        AdminController.removeSection(session, section)
    elif i == 'a':
        # can only add sections with courses current is reg session
        ss = filter(lambda x: x.name not in session.sections, allSections)
        ss = filter(lambda x: x.courseNumber in session.courses, list(ss))
        i, section = selectFromList(
                list(ss),
                Section,
                "select section to add; 'q' to quit")
        if section == None:
            return
        else:
            AdminController.addSection(session, section)
    else:
        return

    printRecord(session, RegistrationSession)
    return


# returns tuple of input and selected record
def selectFromList(records, classRef, prompt):
    record = None
    while True:
        printRecordList(records, classRef)
        i = input(prompt)
        
        try:
            num = int(i)
        except:
            return (i, None)

        try: 
            record = records[num]
            return (i, record)
        except:
            print('that is not a list option')


def recordMenu(classRef):
    stay = True
    while stay:
        allRecords = classRef.fetchAll()
        printRecordList(allRecords, classRef)
        i = input("select record number to edit, delete, or view, 'a' to add: ")
        if i == 'q':
            print('returning to previous menu')
            stay = False
        elif i == 'a':
            inputRecord(classRef)
        else:
            try:
                num = int(i)
            except:
                print('did not understand option')
                continue

            try: 
                rec = allRecords[num]
            except:
                print('that is not a list option')
                continue

            editDeleteRecord(rec, classRef)


def printRecordList(records, classRef):
    print("All current {}s".format(classRef.__name__))
    i = 0
    for r in records:
        print("\t{}: {}".format(i, getattr(r, classRef.nameAttr)))
        i = i + 1

    print("\n")


def buildParams(record, classRef):
    params = {}
    for item, itemtup in classRef.typeSpec.items():
        itemtype, kind = itemtup
        if kind == 'atom':
            if record is None:
                existingItem = None
            else:
                existingItem = getattr(record, item)
            val = inputAtom(existingItem, item, itemtype)
            params[item] = val
        if kind == 'list':
            if record is None:
                existingList = []
            else:
                existingList = getattr(record, item)
            
            newList = inputList(existingList, item, itemtype)
            if len(newList) == 0:
                continue
            else:
                params[item] = newList

    return params


def inputList(existingList, item, atomtype):
    print("edit values for {}".format(item))
    # print the list with keys
    # prompt for one to remove or add
    while True:
        print("current state")
        i = 0
        for item in existingList:
            print("{}: {}".format(i, existingList[i]))
            i = i + 1
        
        key = input("choose an item to delete, 'a' to add, or 'q' to be done: ")
        if key == 'q':
            return existingList
        if key == 'a':
            val = inputAtom("", "new item", atomtype)
            existingList.append(val)
        else:
            try:
                num = int(key)
            except:
                print('did not understand option')
                continue

            try: 
                rec = existingList[num]
            except:
                print('that is not a list option')
                continue
            
            existingList.remove(existingList[num])
            

def inputAtom(existingval, item, itemtype):
    i = input("enter value for {} ({}): ".format(item, existingval))
    val = i or existingval

    # input coersion
    if itemtype == bool:
        if val == 'true':
            val = True
        else:
            val = False

    return itemtype(val)


def inputRecord(classRef):
    isValid = False

    while not isValid:
        params = buildParams(None, classRef) 

        isValid, invalidParams = classRef.validate(params)

    newRecord = classRef(params)
    newRecord.save()

    return classRef.fetch(getattr(newRecord, classRef.nameAttr))


def printRecord(record, classref):
    print("\n\n\t{}\n".format(getattr(record, classref.nameAttr)))
    for item, types in classref.typeSpec.items():
        print("\t{}: {}".format(item, getattr(record, item)))

    return


def editDeleteRecord(record, classref):
    i = input("'e' for edit; 'd' for delete; 'v' for view: ")
    if i == 'd': 
        record.delete()
        return

    if i == 'v':
        printRecord(record, classref)
        return

    if i == 'e':
        params = buildParams(record, classref)
        record.modify(params)
        return

    print("did not understand option")
    return


if __name__ == '__main__':
    run()



