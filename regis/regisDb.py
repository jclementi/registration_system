import mysql.connector
from mysql.connector import errorcode
from regis.database.dbCourses import DbCourses
from regis.database.dbDivisions import DbDivisions
from regis.database.dbPrereqs import DbPrereqs
from regis.database.dbMajors import DbMajors
from regis.database.dbInstructors import DbInstructors
from regis.database.dbInstrAssign import DbInstrAssign
from regis.database.dbInstrDiv import DbInstrDiv
from regis.database.dbStudents import DbStudents
from regis.database.dbRegistrations import DbRegistrations
from regis.database.dbSections import DbSections
from regis.database.dbRegistrationSess import DbRegistrationSess
from regis.database.dbPrevCourses import DbPrevCourses
from regis.database.dbAvailableCourses import DbAvailableCourses
from regis.database.dbDeclaredMajors import DbDeclaredMajors

db_name = 'regis_db'

class RegisDb():
    def __init__(self):
        try:
            db_connection = mysql.connector.connect(user='root', password='root')
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Acess denied/wrong user name or password")

        self.connection = db_connection
        self.cursor = db_connection.cursor()

        try:
            self.connection.database = db_name
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_BAD_DB_ERROR:
                self.createDatabase()
            else:
                print(err)

    def __del__(self):
        self.connection.close()
        self.cursor.close()

    # admin stuff for database
    def createDatabase(self):
        try:
            self.cursor.execute(
                "create database {} default character set 'utf8'".format(db_name))
            self.connection.commit()
        except mysql.connector.Error as err:
            print("Failed creating database: {}".format(err))
            exit(1)

        self.connection.database = db_name

    def deleteDatabase(self):
        try:
            self.cursor.execute("drop database if exists {}".format(db_name))

            self.connection.commit()
        except mysql.connector.Error as err:
            print("Failed deleting database: {}".format(err))
            exit(1)

    def initializeDatabase(self):
        self.deleteDatabase()
        self.createDatabase()

        dbmajors = DbMajors()
        dbdivisions = DbDivisions()
        dbcourses = DbCourses()
        dbprereqs = DbPrereqs()
        dbinstructors = DbInstructors()
        dbinstrAssign = DbInstrAssign()
        dbinstrDiv = DbInstrDiv()
        dbstudents = DbStudents()
        dbregistrations = DbRegistrations()
        dbsections = DbSections()
        dbregsessions = DbRegistrationSess()
        dbavailcourses = DbAvailableCourses()
        dbprevcourses = DbPrevCourses()
        dbdeclaredmajors = DbDeclaredMajors()

        connectors = [
                dbregsessions,
                dbmajors,
                dbdivisions,
                dbcourses,
                dbprereqs,
                dbsections,
                dbstudents,
                dbinstructors,
                dbinstrAssign,
                dbinstrDiv,
                dbregistrations,
                dbavailcourses,
                dbprevcourses,
                dbdeclaredmajors,
        ]

        for c in connectors:
            c.createTable()

        for c in connectors:
            c.insertInitialData()

        # have to stitch on available courses linking table info
        sessparams = DbRegistrationSess().selectByName('autumn 2018')
        DbRegistrationSess().update(sessparams['id'], {'courses': [
            'cmsc150', 'cmsc151', 'phys150', 'phys151']})

        self.connection.commit()

if __name__ == '__main__':
    db = RegisDb()
    db.initializeDatabase()
